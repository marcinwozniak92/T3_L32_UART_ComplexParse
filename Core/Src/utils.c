/*
 * utils.c
 *
 *  Created on: 12 maj 2021
 *      Author: Mateusz Salamon
 */
#include "main.h"
#include "usart.h"
#include "string.h"

void UartLog(char* Message)
{
	HAL_UART_Transmit(&huart2, (uint8_t*)Message, strlen(Message), 1000);
}
